#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <AWS_IOT.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#include "time.h"
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <HTTPClient.h>
#include <Statsd.h>
#include <DHTesp.h>

int pot = A0;    // analog pin to which potentiometer is attached
int value;      // variable to read the value from the analog pin

int sendFrequency = 3000;
int currentTimer = 0;

unsigned long failureEpoch;
bool failureTriggered = false;
bool isFailed = false;
int failureThresholdPct = 90;
int failureTimer;

// use first channel of 16 channels (started from zero)
#define LEDC_CHANNEL_0     0

// use 13 bit precission for LEDC timer
#define LEDC_TIMER_13_BIT  13

// use 5000 Hz as a LEDC base frequency
#define LEDC_BASE_FREQ     5000

// fade LED PIN (replace with LED_BUILTIN constant for built-in LED)
#define LED_PIN            17

// Arduino like analogWrite
// value has to be between 0 and valueMax
void ledcAnalogWrite(uint8_t channel, uint32_t value, uint32_t valueMax = 255) {
  // calculate duty, 8191 from 2 ^ 13 - 1
  uint32_t duty = (8191 / valueMax) * _min(value, valueMax);

  // write duty to LEDC
  ledcWrite(channel, duty);
}

float longChar;
float latChar;
unsigned long epochTime;

bool speedOverride = false;
int speedOverrideValue = 0;
float tempOverrideValue = 0;
bool demoOneStatus = true;
bool demoTwoStatus = false;
unsigned long demoEpoch;
int demoEpochStep = 10;
bool demoStepDirection = false;
int demoStepLimit = 0;
int demoStepCount = 0;
int demoOneFailureOverride = false;
bool warmupPeriod = false;

DHTesp dht;
int dhtPin = 16;

// Define NTP Client to get time
WiFiUDP ntpUDP;
WiFiUDP udp;  // or EthernetUDP, as appropriate.
Statsd statsd(udp, "172.20.10.8", 8125);
NTPClient timeClient(ntpUDP);

// Variables to save date and time
String formattedDate;
String dayStamp;
String timeStamp;

//const size_t bufferSize = JSON_ARRAY_SIZE(1) + JSON_OBJECT_SIZE(3) + JSON_OBJECT_SIZE(8);
//DynamicJsonBuffer jsonBuffer(bufferSize);

//const size_t capacity = JSON_OBJECT_SIZE(2) + 30;
//DynamicJsonBuffer jsonBufferMessage(capacity);

const size_t capacityMessage = JSON_OBJECT_SIZE(2) + 30;
DynamicJsonDocument jsonBufferMessage(capacityMessage);

//const size_t capacity = JSON_ARRAY_SIZE(1) + JSON_OBJECT_SIZE(3) + JSON_OBJECT_SIZE(8) + 420;
//DynamicJsonDocument doc(capacity);
DynamicJsonDocument doc(1024);

#ifdef __cplusplus
extern "C" {
#endif

  uint8_t temprature_sens_read();
  //uint8_t g_phyFuns;

#ifdef __cplusplus
}
#endif

uint8_t temp_farenheit;
float temp_celsius;
char buf[256];


// include your personal WiFi and AWS configuration. 
#include "config.h"

AWS_IOT hornbill;

int status = WL_IDLE_STATUS;
int tick = 0, msgCount = 0, msgReceived = 0;
char payload[512];
char rcvdPayload[512];


// Motor A
int motor1Pin1 = 27; 
int motor1Pin2 = 26; 
int enable1Pin = 14; 
// Setting PWM properties
const int freq = 30000;
const int pwmChannel = 5;
const int resolution = 8;
int dutyCycle = 230;


void mySubCallBackHandler(char *topicName, int payloadLen, char *payLoad)
{
  strncpy(rcvdPayload, payLoad, payloadLen);
  rcvdPayload[payloadLen] = 0;
  msgReceived = 1;
}

void setup()
{
  // Setup timer and attach timer to a led pin
  ledcSetup(LEDC_CHANNEL_0, LEDC_BASE_FREQ, LEDC_TIMER_13_BIT);
  ledcAttachPin(LED_PIN, LEDC_CHANNEL_0);

  Serial.begin(115200);
  
  while (status != WL_CONNECTED)
  {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(WIFI_SSID);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
    status = WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

    // wait 5 seconds for connection:
    delay(5000);
  }
  Serial.println("Connected to wifi");

  
  longChar = -95.559563;
  latChar = 29.781836;

  // Initialize a NTPClient to get time
  timeClient.begin();
  // Set offset time in seconds to adjust for your timezone, for example:
  // GMT +1 = 3600
  // GMT +8 = 28800
  // GMT -1 = -3600
  // GMT 0 = 0
  //timeClient.setTimeOffset(-21600);
  timeClient.setTimeOffset(0);
  while (!timeClient.update()) {
    timeClient.forceUpdate();
  }
  // The formattedDate comes with the following format:
    // 2018-05-28T16:00:13Z
    // We need to extract date and time
  formattedDate = timeClient.getFormattedDate();
  epochTime = timeClient.getEpochTime();
  Serial.println(formattedDate);

  if (hornbill.connect(HOST_ADDRESS, CLIENT_ID, aws_root_ca_pem, certificate_pem_crt, private_pem_key) == 0)
  {
    Serial.println("Connected to AWS");
    delay(1000);

    if (0 == hornbill.subscribe(TOPIC_NAME_SUBSCRIBE, mySubCallBackHandler))
    {
      Serial.println("Subscribe Successfull");
    }
    else
    {
      Serial.println("Subscribe Failed, Check the Thing Name and Certificates");
      while (1);
    }
  }
  else
  {
    Serial.println("AWS connection failed, Check the HOST Address");
    while (1);
  }

  failureTimer = random(120, 180);

  // sets the pins as outputs:
  pinMode(motor1Pin1, OUTPUT);
  pinMode(motor1Pin2, OUTPUT);
  pinMode(enable1Pin, OUTPUT);

  dht.setup(dhtPin, DHTesp::DHT22);
  
  // configure LED PWM functionalitites
  ledcSetup(pwmChannel, freq, resolution);
  
  // attach the channel to the GPIO to be controlled
  ledcAttachPin(enable1Pin, pwmChannel);

  delay(2000);
}

void loop()
{
  //Serial.println("*********************************");
  float temp = dht.toFahrenheit(dht.getTemperature());
  float humidity = dht.getHumidity();
  float hi = dht.computeHeatIndex(temp,humidity,true);

//  Serial.print("Air Temperature: ");
//  Serial.println(temp);
//  Serial.print("Air Humidity: ");
//  Serial.println(humidity);
//  Serial.print("Heat Index: ");
//  Serial.println(hi);
  
  temp_farenheit = temprature_sens_read();
  
  int calcPct;
  int calcValue;


  if (speedOverride == true)
  {
    calcPct = speedOverrideValue;
    calcValue = speedOverrideValue * 2.55;
  }
  else
  {
    value = analogRead(pot);             // reads the value of potentiometer (ranging from 0 and 1023)
    //Serial.println("Raw Value:" + value);
    value = map(value, 0, 1023, 0, 255); // scale it to use it with analogWrite
    calcValue = value / 4;
    calcPct = ((float)calcValue / 255) * 100;
  }
  

  if ((demoOneStatus || demoTwoStatus) && tempOverrideValue == 0)
  {
    demoEpoch = timeClient.getEpochTime();
    tempOverrideValue = 120;
    speedOverrideValue = 40;
    isFailed = false;
    failureTriggered = false;
    demoStepLimit = random(0,45);
    demoStepDirection = true;
    
    warmupPeriod = true;
    
    if (demoOneStatus)
    {
      demoOneFailureOverride = true;
    }
    else
    {
      demoOneFailureOverride = false;
    }
  }
  if (demoOneStatus || demoTwoStatus)
  {
    unsigned long currentEpoch = timeClient.getEpochTime();
    unsigned long epochDiff = currentEpoch - demoEpoch;
    if (epochDiff > demoEpochStep)
    {
      warmupPeriod = false;
      if (demoStepDirection)
      {
        tempOverrideValue++;
      }
      else
      {
        if (tempOverrideValue > 120)
        {
          tempOverrideValue--;
        }
      }
      if (tempOverrideValue < 125)
      {
        speedOverrideValue = 40;
        demoEpoch = timeClient.getEpochTime();
        demoStepCount++;
      }
      else if (tempOverrideValue >= 125 && tempOverrideValue < 130)
      {
        speedOverrideValue = 55;
        demoEpoch = timeClient.getEpochTime();
        demoStepCount++;
      }
      else if (tempOverrideValue >= 130 && tempOverrideValue < 135)
      {
        speedOverrideValue = 65;
        demoEpoch = timeClient.getEpochTime();
        demoStepCount++;
      }
      else if (tempOverrideValue >= 135 && tempOverrideValue < 140)
      {
        speedOverrideValue = 75;
        demoEpoch = timeClient.getEpochTime();
        demoStepCount++;
      }
      else if (tempOverrideValue >= 140 && tempOverrideValue < 145)
      {
        speedOverrideValue = 85;
        demoEpoch = timeClient.getEpochTime();
        demoStepCount++;
      }
      else if (tempOverrideValue >= 145 && tempOverrideValue < 150)
      {
        speedOverrideValue = 95;
        demoEpoch = timeClient.getEpochTime();
        demoStepCount++;
      }
      else
      {
        if (tempOverrideValue >= 175)
        {
          tempOverrideValue = 175;
        }
        speedOverrideValue = 100;
        demoEpoch = timeClient.getEpochTime();
        demoStepCount++;
      }
      if ((demoStepCount >= demoStepLimit || tempOverrideValue >= 155) && !demoTwoStatus)
      {
        demoStepCount = 0;
        demoStepLimit = random(0,45);
        demoStepDirection = !demoStepDirection;
      }
    }
//    Serial.print("Demo temp: ");
//    Serial.println(tempOverrideValue);
//    Serial.print("Demo speed: ");
//    Serial.println(speedOverrideValue);
//    Serial.print("Demo Step Count: ");
//    Serial.print(demoStepCount);
//    Serial.print("/");
//    Serial.println(demoStepLimit);

    temp_farenheit = tempOverrideValue;
    calcPct = speedOverrideValue;
  }


  if (calcPct >= failureThresholdPct && !failureTriggered)
  {
    Serial.println("Initial failure state triggered");
    failureEpoch = timeClient.getEpochTime();
    failureTriggered = true;
    //ledcWrite(LEDC_CHANNEL_0, 255); 
    ledcAnalogWrite(LEDC_CHANNEL_0, 255);
  }
  else if (calcPct < failureThresholdPct)
  {
    failureEpoch = 0;
    failureTriggered = false;
    //ledcWrite(LEDC_CHANNEL_0, 0); 
    ledcAnalogWrite(LEDC_CHANNEL_0, 0);
  }
  if (failureTriggered && !isFailed)
  {
    unsigned long currentEpoch = timeClient.getEpochTime();
    unsigned long epochDiff = currentEpoch - failureEpoch;
    Serial.print("Current failure interval:");
    Serial.print(epochDiff);
    Serial.print("/");
    Serial.println(failureTimer);
    if (epochDiff >= failureTimer)
    {
      isFailed = true;
      Serial.println("Failure activated");
    }
  }

  if (isFailed)
  {
    if (!demoOneFailureOverride)
    {
      calcPct = 0;
      calcValue = 0;
      //ledcWrite(LEDC_CHANNEL_0, 255); 
      ledcAnalogWrite(LEDC_CHANNEL_0, 255);
    } 
  }
  

  //Motor Speed
  // Move the DC motor forward at maximum speed
  digitalWrite(motor1Pin1, LOW);
  digitalWrite(motor1Pin2, HIGH); 
  if ( calcPct > 0)
  {
    //ledcAnalogWrite(pwmChannel, 0);
    //dutyCycle = (255 * calcPct) / 100;
    //dutyCycle = 245 + (0.1 * calcPct);
    dutyCycle = 254;
    //Serial.println(dutyCycle);
    ledcWrite(pwmChannel, dutyCycle); 
    //ledcAnalogWrite(pwmChannel, dutyCycle);
  }
  else
  {
    //dutyCycle = 0;
    dutyCycle = 254;
    //Serial.println(dutyCycle);
    //ledcWrite(pwmChannel, dutyCycle);
    ledcAnalogWrite(pwmChannel, dutyCycle);
  }

  if (currentTimer >= sendFrequency) {
    DynamicJsonDocument doc(2048);
    // The formattedDate comes with the following format:
    // 2018-05-28T16:00:13Z
    // We need to extract date and time
    formattedDate = timeClient.getFormattedDate();
    unsigned long currentEpoch = timeClient.getEpochTime();

    unsigned long epochDiff = currentEpoch - epochTime;

    doc["querytype"] = "Update";
    doc["witsmlwellboreuid"] = "slalom-demo-wellbore";
    doc["witsmlwelluid"] = "slalom-demo";
    doc["indextype"] = "date time";
    doc["loguid"] = "time-log-arduino";
    doc["indexcurve"] = "TIME";
    doc["witsmlserverurl"] = "http://WITSML-769574829.us-west-2.elb.amazonaws.com/witsml/witsmlstore.svc";

    char latCharBuffer[10];
    char longCharBuffer[10];
    dtostrf(latChar,5, 3, latCharBuffer);
    dtostrf(longChar,5, 3, longCharBuffer);
    String dataRow = formattedDate + "," + temp_farenheit + "," + calcPct + "," + latCharBuffer + "," + longCharBuffer + "," + epochDiff;

    JsonObject logdatarows = doc.createNestedObject("logdatarows");
    logdatarows["uomrow"] = "s,f,%,dega,dega,s";
    JsonArray logdatarows_datarows = logdatarows.createNestedArray("datarows");
    logdatarows_datarows.add(dataRow);
    Serial.println(dataRow);
    logdatarows["mnemonicrow"] = "TIME,TEMP,FANSPEED,LAT,LONG,UPTIME";

    String rootstr;
    serializeJson(doc, rootstr);
    //doc.printTo(rootstr);
    rootstr.replace("\"", "\\\"");
    rootstr = "\"" + rootstr + "\"";
    sprintf(payload, rootstr.c_str());


    
    if (hornbill.publish(TOPIC_NAME, payload) == 0)
    {
      Serial.print("Publish Message:");
      Serial.println(payload);
    }
    else
    {
      Serial.println("Publish failed");
      Serial.println(payload);
    }


    //Serial.println("Publishing to StatsD");
    sendGaugeToStatsD("device.fan.speed", calcPct, "city:houston,location:slalom_citycentre,floor:13,device_name:Slalom-BOARD-1");
    sendGaugeToStatsD("device.sensor.temperature", temp_farenheit, "city:houston,location:slalom_citycentre,floor:13,device_name:Slalom-BOARD-1");
    sendGaugeToStatsD("device.sensor.uptime", epochDiff, "city:houston,location:slalom_citycentre,floor:13,device_name:Slalom-BOARD-1");
    sendGaugeToStatsD("device.sensor.air.humidity", humidity, "city:houston,location:slalom_citycentre,floor:13,device_name:Slalom-BOARD-1");
    sendGaugeToStatsD("device.sensor.air.temperature", temp, "city:houston,location:slalom_citycentre,floor:13,device_name:Slalom-BOARD-1");
    sendGaugeToStatsD("device.sensor.air.heatindex", hi, "city:houston,location:slalom_citycentre,floor:13,device_name:Slalom-BOARD-1");
    if (isFailed)
    {
      sendGaugeToStatsD("device.fan.status", 0, "city:houston,location:slalom_citycentre,floor:13,device_name:Slalom-BOARD-1");
    }
    else
    {
      sendGaugeToStatsD("device.fan.status", 1, "city:houston,location:slalom_citycentre,floor:13,device_name:Slalom-BOARD-1");
    }
    
    //Serial.println("Finished publishing to StatsD");
    currentTimer = 0;
  }
  if (msgReceived == 1)
  {
    msgReceived = 0;
    Serial.print("Received Message:");
    const char* json = rcvdPayload;
    //JsonObject& root = jsonBufferMessage.parseObject(json);
    deserializeJson(jsonBufferMessage, json);
    const char* STATUS = jsonBufferMessage["STATUS"]; // "ON"
    const char* SPEED = jsonBufferMessage["SPEED"]; // "0"
    if (strcmp(STATUS, "OFF") == 0)
    {
      speedOverride = true;
      speedOverrideValue = 0;
      demoOneStatus = false;
      demoTwoStatus = false;
    }
    else if (strcmp(STATUS, "DEMO1") == 0)
    {
      demoOneStatus = true;
      demoTwoStatus = false;
      tempOverrideValue = 0;
      speedOverrideValue = 0;
      demoStepDirection = true;
    }
    else if (strcmp(STATUS, "DEMO2") == 0)
    {
      demoOneStatus = false;
      demoTwoStatus = true;
      tempOverrideValue = 0;
      speedOverrideValue = 0;
      demoStepDirection = true;
    }
    else
    {
      Serial.println("Setting speedOverride to false");
      speedOverride = false;
      if (strcmp(SPEED, "SENSOR") == 0)
      {
        speedOverride = false;
        demoOneStatus = false;
        demoTwoStatus = false;
      }
      else
      {
        speedOverride = true;
        demoOneStatus = false;
        demoTwoStatus = false;
        speedOverrideValue = atoi(SPEED);
      }
    }
  }

  currentTimer += 1000;
  delay(1000);
}

void sendGaugeToStatsD(String aMetric, float aVal, String aTags)
{
  statsd.setTagStyle(TAG_STYLE_DATADOG);
  statsd.begin();
  statsd.gauge(aMetric, aVal, aTags, 1.0);
}
